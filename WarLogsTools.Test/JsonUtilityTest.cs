﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using WarLogsTools.Test.Resources;
using WarLogsTools.Utility;

namespace WarLogsTools.Test
{
    namespace Utility
    {
        [TestFixture]
        public class JsonUtilityTest
        {
            [Test]
            public void GetFromJson()
            {
                string json = TestUtility.TestUtility.GetStringResource("testClass");
                var actual = JsonUtility.GetFromJson<List<TestClass>>(json);
                Assert.NotNull(actual);
                TestClass first = actual.FirstOrDefault();
                Assert.NotNull(first);
                Assert.AreEqual(42, first.value1);
                Assert.AreEqual("someString", first.value2);
            }
        }
    }
}