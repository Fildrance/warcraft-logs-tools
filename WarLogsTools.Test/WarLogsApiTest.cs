﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using WarLogsTools.Model.Classes;
using WarLogsTools.Model.RaidData;
using WarLogsTools.Model.Ranks;
using WarLogsTools.Model.Zones;

namespace WarLogsTools.Test
{
    namespace Integration
    {
        [TestFixture]
        public class WarLogsApiTest
        {
            [SetUp]
            public void Setup()
            {
                _warLogsApi = new WarLogsApiClient("0d17c1cc1721b00fcc1c36f097e36247");
            }

            private IWarLogsApi _warLogsApi;

            [Test]
            public void GetRankingsAsyncTest()
            {
                //arrange
                var encounter = new ZoneInfo {Id = "1721"};
                //act
                Rankings actual = _warLogsApi.GetRankingsAsync(encounter.Id).Result;
                //assert
                Assert.NotNull(actual);
                Assert.AreEqual(200, actual.Rankers.Length);
                Assert.IsTrue(actual.Rankers.First().Total > actual.Rankers.Last().Total);
                var Volathor = actual.Rankers.Single(x => x.Name == "Volathor");
                Assert.AreEqual(new DateTime(2015, 6, 12), Volathor.FightStartTime.Date);
            }

            [Test]
            public void GetZoneDatasAsyncTest()
            {
                //arrange

                //act
                ZoneInfo[] actual = _warLogsApi.GetZoneInfoAsync().Result.ToArray();
                ZoneInfo highMaul = actual.First(x => x.Id == "6");
                Bracket firstBracket = highMaul.Brackets.FirstOrDefault();
                Bracket lastBracket = highMaul.Brackets.LastOrDefault();
                Encounter kargathEncounter = highMaul.Encounters.FirstOrDefault();
                //assert
                Assert.NotNull(actual);
                Assert.NotNull(firstBracket);
                Assert.NotNull(lastBracket);
                Assert.NotNull(kargathEncounter);
                Assert.AreEqual(6, actual.Count());
                Assert.AreEqual("Highmaul", highMaul.Name);
                Assert.AreEqual("1", firstBracket.Id);
                Assert.AreEqual("0-647 Item Level", firstBracket.Name);
                Assert.AreEqual("5", lastBracket.Id);
                Assert.AreEqual("693+ Item Level", lastBracket.Name);
                Assert.AreEqual(7, highMaul.Encounters.Length);
                Assert.AreEqual(1721, kargathEncounter.EncounterId);
                Assert.AreEqual("Kargath Bladefist", kargathEncounter.Name);
            }

            [Test]
            public void GetClassesDatasAsyncTest()
            {
                //arrange

                //act
                Task<IEnumerable<ClassInfo>> data = _warLogsApi.GetClassesDatasAsync();
                IEnumerable<ClassInfo> actual = data.Result;
                //assert
                Assert.NotNull(actual);
                Assert.True(actual.Any(x=>x.Name == "Priest"));
                //Assert.True(actual.Any(x=>x.ClassSpecs.Any(s=>s.Name == "Discipline"))); TODO: 
            }

            [Test]
            public void GetRaidReportInfosAsyncTest_NoPeriodUsed()
            {
                //arrange

                //act
                var data = _warLogsApi.GetRaidReportInfosAsync("Забытое братство", "EU", "ясеневыи-лес").Result;
                //assert
                Assert.NotNull(data);
                var firstData = data.First();
                Assert.DoesNotThrow(()=> { var v = firstData.StartTime; });
            }


            [Test]
            public void GetRaidReportInfoAsyncTest()
            {
                //arrange
                var timeStart = new DateTime(2015,6,2);
                var timeEnd = new DateTime(2015,6,3);
                //act
                var data = _warLogsApi.GetRaidReportInfosAsync("Забытое братство", "EU", "ясеневыи-лес", timeStart, timeEnd).Result;
                //assert
                Assert.AreEqual(1, data.Count());
                Assert.AreEqual("scorpich", data.First().Owner);
            }

            [Test]
            public void GetRaidReportAsync()
            {
                //arrange

                //act
                var data = _warLogsApi.GetRaidReportAsync("6N78cvw4qTmkapFj").Result;
                //assert
                Assert.NotNull(data);
                Assert.AreEqual(data.RaidFights.Length, 54);
                var count = data.RaidFights.Where(x => x.BossId != 0);
                Assert.NotNull(count);
                var count2 = data.Enemies.Where(x => x.Type == EnemyType.Boss);
                Assert.NotNull(count2);
            }

        }
    }
}