﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WarLogsTools.Model;
using WarLogsTools.Model.Classes;
using WarLogsTools.Model.RaidData;
using WarLogsTools.Model.Ranks;
using WarLogsTools.Model.Zones;

namespace WarLogsTools
{
    public interface IWarLogsApi
    {
        /// <summary>
        /// Get every zoneData for warcraftlogs
        /// </summary>
        /// <returns>Enumeration of zone data</returns>
        Task<IEnumerable<ZoneInfo>> GetZoneInfoAsync();

        /// <summary>
        /// Get rankings for encounter
        /// </summary>
        /// <param name="encounterId">Id of encounter from Zone info that you need rankings for. <see cref="GetZoneInfoAsync"/></param>
        /// <returns>Rankings list for encounter</returns>
        Task<Rankings> GetRankingsAsync(string encounterId);

        /// <summary>
        /// Get warcraftlogs classes info
        /// </summary>
        /// <returns>Enumeration of class info from WarcraftLogs</returns>
        Task<IEnumerable<ClassInfo>> GetClassesDatasAsync();

        /// <summary>
        /// Get list of reports that belong to a certain guild and had have report start date in certain period. If start date and end date is missing - warcraftlogs policy for period choice i
        /// </summary>
        /// <param name="warlogsGuild">Name of the guild on warcraftlogs.com</param>
        /// <param name="region">region name (in 2 symbols)</param>
        /// <param name="serverSlugName">'slug' name of server. Means that some characters are replaced, space is replaced by '-' and everything in lower case</param>
        /// <param name="startDate">Find reports that have start time after this date</param>
        /// <param name="endDate">Find reports that have start time before this date</param>
        /// <returns>List of RaidReport info, containing report overall info and reportId for further report investigation through <see cref="GetRaidReportAsync"/></returns>
        Task<IEnumerable<RaidReportInfo>> GetRaidReportInfosAsync(string warlogsGuild, string region, string serverSlugName,
            DateTime? startDate = null, DateTime? endDate = null);

        /// <summary>
        /// Gets Raid report, containing raid fights data.
        /// </summary>
        /// <param name="reportId">ReportId from <see cref="GetRaidReportInfosAsync"/></param>
        /// <returns>Raid report</returns>
        Task<RaidReport> GetRaidReportAsync(string reportId);
    }
}