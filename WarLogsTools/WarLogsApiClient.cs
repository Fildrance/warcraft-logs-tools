﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarLogsTools.Model.Classes;
using WarLogsTools.Model.RaidData;
using WarLogsTools.Model.Ranks;
using WarLogsTools.Model.Zones;
using WarLogsTools.Properties;
using WarLogsTools.Utility;

namespace WarLogsTools
{
    /// <summary>
    /// Warcraft logs client for web api through json
    /// </summary>
    public class WarLogsApiClient : IWarLogsApi
    {
        #region
        private readonly string _hostWebApi;
        private readonly string _webApiVersion;
        private readonly string _apiKey;
        #endregion

        #region c-tor
        public WarLogsApiClient(string apiKey)
        {
            if(string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentNullException("apiKey");

            _hostWebApi = Resources.WebApiHostUrl;
            _webApiVersion = Resources.WebApiVersion;

            _apiKey = apiKey;
        }
        #endregion

        #region interface impl
        public async Task<IEnumerable<ZoneInfo>> GetZoneInfoAsync()
        {
            string path = "zones?";
            return await GetFromWebApi<List<ZoneInfo>>(path, _apiKey);
        }

        public async Task<Rankings> GetRankingsAsync(string encounterId)
        {
            string path = string.Format("rankings/encounter/{0}?", encounterId);
            return await GetFromWebApi<Rankings>(path, _apiKey);
        }

        public async Task<IEnumerable<ClassInfo>> GetClassesDatasAsync()
        {
            string path = "classes?";
            return await GetFromWebApi<List<ClassInfo>>(path, _apiKey);
        }

        public async Task<RaidReport> GetRaidReportAsync(string reportId)
        {
            string path = string.Format("report/fights/{0}?", reportId);
            return await GetFromWebApi<RaidReport>(path, _apiKey);
        }

        public async Task<IEnumerable<RaidReportInfo>> GetRaidReportInfosAsync(string warlogsGuild, string region,
            string serverSlugName, DateTime? startDate = null, DateTime? endDate = null)
        {
            var path =
                new StringBuilder(string.Format("reports/guild/{0}/{1}/{2}", warlogsGuild, serverSlugName, region));
            path.Append("?");
            if (startDate.HasValue || endDate.HasValue)
            {
                if (startDate.HasValue)
                {
                    path.Append("start=");
                    path.Append(DateHelper.ToUnixTime(startDate.Value));
                }
                if (startDate.HasValue && endDate.HasValue)
                {
                    path.Append("&");
                }
                if (endDate.HasValue)
                {
                    path.Append("end=");
                    path.Append(DateHelper.ToUnixTime(endDate.Value));
                }
                path.Append("&");
            }
            return await GetFromWebApi<IEnumerable<RaidReportInfo>>(path.ToString(), _apiKey);
        }
        #endregion

        #region private methods

        private async Task<T> GetFromWebApi<T>(string requestQuery, string apiKey)
        {
            string path = string.Format("{0}/{1}/{2}api_key={3}", _hostWebApi, _webApiVersion, requestQuery, apiKey);
            string responceJson = await HttpUtility.GetResponceAsync(path);
            return JsonUtility.GetFromJson<T>(responceJson);
        }

        #endregion
    }
}