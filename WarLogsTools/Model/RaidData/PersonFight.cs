﻿namespace WarLogsTools.Model.RaidData
{
    /// <summary>
    /// Record of character taken part in raid fight
    /// </summary>
    public class PersonFight
    {
        /// <summary>
        /// RaidFight id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Battle ressurection TODO: check if it really is
        /// </summary>
        public int Instances { get; set; }
    }
}
