﻿using Newtonsoft.Json;

namespace WarLogsTools.Model.RaidData
{
    public class RaidFight
    {
        private byte _difficulty;
        [JsonProperty("end_time")] private long _end;
        [JsonProperty("start_time")] private long _start;

        /// <summary>
        /// Raid fight Id, counts from 0 and on
        /// </summary>
        [JsonProperty("id")]
        public int FightId { get; set; }

        /// <summary>
        /// Encounter id from warcraft logs zone data
        /// </summary>
        [JsonProperty("boss")]
        public int BossId { get; set; }

        /// <summary>
        /// Returns true if fight ended with boss kill or winning encounter
        /// </summary>
        public bool Kill { get; set; }

        /// <summary>
        /// Encounter name or enemy name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Raid size for raidFight
        /// </summary>
        [JsonProperty("size")]
        public int RaidSize { get; set; }

        /// <summary>
        /// Mark for difficulty
        /// </summary>
        public virtual byte Difficulty
        {
            get { return _difficulty; }
        }

        /// <summary>
        /// Fight start time (as period from start of raid report)
        /// </summary>
        public virtual long Start
        {
            get { return _start; }
        }

        /// <summary>
        /// Fight end time (as period from start of raid report)
        /// </summary>
        public virtual long End
        {
            get { return _end; }
        }
    }
}