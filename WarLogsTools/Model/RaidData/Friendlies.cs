﻿namespace WarLogsTools.Model.RaidData
{
    /// <summary>
    /// Friendly target in raid fight
    /// </summary>
    public class Friendlies
    {
        /// <summary>
        /// Collection of raid fight that person had taken part in
        /// </summary>
        public PersonFight[] Fights;
        /// <summary>
        /// Friendly target name
        /// </summary>
        public string Name { get; set; }


    }
}