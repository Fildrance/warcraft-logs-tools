﻿using Newtonsoft.Json;

namespace WarLogsTools.Model.RaidData
{
    /// <summary>
    /// Raid fight report data
    /// </summary>
    public class RaidReport
    {
        /// <summary>
        /// If log is for challange mode - returns true
        /// </summary>
        [JsonProperty("challengeModes")]
        public bool IsChallangeModesLog { get; set; }
        /// <summary>
        /// Collection of raidFights
        /// </summary>
        [JsonProperty("fights")]
        public RaidFight[] RaidFights { get; set; }
        /// <summary>
        /// Collection of friendly targets, that was recorded in report
        /// </summary>
        public Friendlies[] Friendlies { get; set; }
        /// <summary>
        /// Collection of enemy targets, that was recorded in report
        /// </summary>
        public Enemies[] Enemies { get; set; }
    }
}
