﻿using System;
using Newtonsoft.Json;

namespace WarLogsTools.Model.RaidData
{
    /// <summary>
    /// Unfriendly target in raid fight
    /// </summary>
    public class Enemies
    {
        [JsonProperty("type")] private string _type;

        /// <summary>
        /// Name of target
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Guid of target.
        /// </summary>
        public int Guid { get; set; }

        /// <summary>
        /// Id of target
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Array of fight instances that enemy was part of
        /// </summary>
        public PersonFight[] Fights { get; set; }

        /// <summary>
        /// Type of enemy
        /// </summary>
        public virtual EnemyType Type
        {
            get { return (EnemyType) Enum.Parse(typeof(EnemyType), _type,true); }
        }
    }

    /// <summary>
    /// Type of enemies
    /// </summary>
    public enum EnemyType
    {
        /// <summary>
        /// default value
        /// </summary>
        None,
        /// <summary>
        /// target is npc, trash mob
        /// </summary>
        NPC,
        /// <summary>
        /// target is raid boss
        /// </summary>
        Boss
    }
}