﻿using System;
using Newtonsoft.Json;
using WarLogsTools.Utility;

namespace WarLogsTools.Model.RaidData
{
    /// <summary>
    /// Raid report description - contains small amount of 
    /// </summary>
    public class RaidReportInfo
    {
        [JsonProperty("end")]
        private long _endTime;
        [JsonProperty("start")]
        private long _startTime;

        /// <summary>
        /// Report Id. Can be user to get <see cref="RaidReport"/>
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Report title, given by owner or auto assigned.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Recorder of report, or person that posted report
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Report recording start time
        /// </summary>
        public virtual DateTime StartTime
        {
            get { return DateHelper.FromUnixTime(_startTime); }
        }

        /// <summary>
        /// Report recording end time
        /// </summary>
        public virtual DateTime EndTime
        {
            get { return DateHelper.FromUnixTime(_endTime); }
        }

        /// <summary>
        /// Raid zone, in which report recording have started. WARNING! Report can contain data from other raid zone
        /// </summary>
        [JsonProperty("zone")]
        public byte ZoneId { get; set; }
    }
}
