﻿using WarLogsTools.Model.Ranks;

namespace WarLogsTools.Model.Zones
{
    /// <summary>
    /// ZoneInfo for warcraftlogs
    /// </summary>
    public class ZoneInfo
    {
        /// <summary>
        /// List of brackets that are actual for this zone
        /// </summary>
        public Bracket[] Brackets { get; set; }
        /// <summary>
        /// Encounters of this zone
        /// </summary>
        public Encounter[] Encounters { get; set; }
        /// <summary>
        /// Zone Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Zone Name
        /// </summary>
        public string Name { get; set; }
    }
}