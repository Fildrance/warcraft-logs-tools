﻿using Newtonsoft.Json;

namespace WarLogsTools.Model.Zones
{
    /// <summary>
    /// Zone encounter
    /// </summary>
    public class Encounter
    {
        /// <summary>
        /// Encounter Id
        /// </summary>
        [JsonProperty("id")]
        public int EncounterId { get; set; }
        /// <summary>
        /// Encounter Name
        /// </summary>
        public string Name { get; set; }
    }
}