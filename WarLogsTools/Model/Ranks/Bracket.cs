﻿namespace WarLogsTools.Model.Ranks
{
    /// <summary>
    /// Bracket info - groups by item level range
    /// </summary>
    public class Bracket
    {
        /// <summary>
        /// Bracket id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Bracket range
        /// </summary>
        public string Name { get; set; }
    }
}