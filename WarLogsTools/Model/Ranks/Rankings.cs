﻿using Newtonsoft.Json;

namespace WarLogsTools.Model.Ranks
{
    /// <summary>
    /// WarcraftLogs rankings
    /// </summary>
    public class Rankings
    {
        /// <summary>
        /// List of warcraftLogs rankings
        /// </summary>
        [JsonProperty("rankings")]
        public Ranker[] Rankers { get; set; }
    }
}