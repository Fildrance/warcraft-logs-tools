﻿using System;
using Newtonsoft.Json;
using WarLogsTools.Utility;

namespace WarLogsTools.Model.Ranks
{
    /// <summary>
    ///     Example of character from rankings
    /// </summary>
    public class Ranker
    {
        [JsonProperty("startTime")] private long _startTime;

        /// <summary>
        ///     WarcraftLogs class id
        /// </summary>
        [JsonProperty("class")]
        public int ClassId { get; set; }

        /// <summary>
        ///     Fight duration (in milliseconds)
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        ///     Warcraftlogs report id for report in which user got rank
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        ///     Fight number during report
        /// </summary>
        public int FightId { get; set; }

        /// <summary>
        ///     Ranker item level
        /// </summary>
        public int ItemLevel { get; set; }

        /// <summary>
        ///     Raid size for ranker raid fight
        /// </summary>
        [JsonProperty("size")]
        public int RaidSize { get; set; }

        /// <summary>
        ///     WarcraftLogs spec id
        /// </summary>
        [JsonProperty("spec")]
        public int SpecId { get; set; }

        /// <summary>
        ///     Server name of ranked player
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        ///     Name of guild of ranked player
        /// </summary>
        public string Guild { get; set; }

        /// <summary>
        ///     Total value (hp healed, dps done, dmg evaded - for healer, dps, tank)
        /// </summary>
        public double Total { get; set; }

        /// <summary>
        ///     Name of ranked person
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Raid fight start date-time in which player got rank
        /// </summary>
        public virtual DateTime FightStartTime
        {
            get { return DateHelper.FromUnixTime(_startTime); }
        }
    }
}