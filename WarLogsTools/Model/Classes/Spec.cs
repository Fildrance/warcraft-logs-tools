﻿namespace WarLogsTools.Model.Classes
{
    /// <summary>
    /// WarcraftLogs spec representation
    /// </summary>
    public class Spec
    {
        /// <summary>
        /// Spec id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Spec name
        /// </summary>
        public string Name { get; set; }
    }
}