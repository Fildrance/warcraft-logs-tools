﻿namespace WarLogsTools.Model.Classes
{
    /// <summary>
    /// ClassInfo instance
    /// </summary>
    public class ClassInfo
    {
        /// <summary>
        /// WarcraftLogs classId
        /// </summary>
        public int ClassId { get; set; }
        /// <summary>
        /// Class name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Specs for class
        /// </summary>
        public Spec[] ClassSpecs { get; set; }
    }
}