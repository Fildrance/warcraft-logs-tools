﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("WarcraftLogsApi")]
[assembly: AssemblyDescription(".NET API for warcraftlogs site can uses not only their public api, but json data that site uses by itself to represent data. Model is very clumsy for now, to be modified later")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Lemon Inc")]
[assembly: AssemblyProduct("WarcraftLogsApi")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-GB")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.12.0")]
[assembly: AssemblyFileVersion("1.0.12-alpha")]
[assembly: InternalsVisibleTo("WarLogsTools.Test")]