﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WarLogsTools.Model;

namespace WarLogsTools.Utility
{
    public static class HttpUtility
    {
        internal static async Task<string> GetResponceAsync(string url)
        {
            var client = new HttpClient();
            return await client.GetStringAsync(url);
        }

    }
}