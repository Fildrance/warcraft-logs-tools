﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WarLogsTools.Utility
{
    internal static class JsonUtility
    {
        internal static T GetFromJson<T>(string jsonString)
        {
            var contractResolver = new DefaultContractResolver();
            contractResolver.DefaultMembersSearchFlags |= BindingFlags.NonPublic;
            var settings = new JsonSerializerSettings { ContractResolver = contractResolver };

            return JsonConvert.DeserializeObject<T>(jsonString, settings);
        }
    }
}
